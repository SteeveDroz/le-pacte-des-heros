# Changelog

## [v1.0.0]

- Création de la règle du jeu
- Création de la fiche de personnage
- Création des objets
- Création d'une partie clé en main

[v1.0.0]: https://gitlab.com/SteeveDroz/le-pacte-des-heros/compare/v0.0.0...v1.0.0
