# Contribuer au Pacte des Héros

## Versionner les fichiers LibreOffice

Afin de pouvoir correctement versionner les documents LibreOffice, vous devez avoir tapé cette ligne de commande **avant votre premier commit**:

`git config diff.zip.textconv unzip -c -a`

Elle vous permettra de décompresser automatiquement les fichiers zip avant de les versionner (les fichiers `.odt` sont en fait des fichiers zip).

Cette ligne n'a besoin d'être tapée qu'une seule fois dans le projet.

## Qui peut participer ?

**TOUT LE MONDE !!!** Même si vous avez des doutes, les outils proposés par GitLab permettent sans problème de guider les néophytes. Vous n'y connaissez rien (ou pas grand chose) et désirez essayer de contribuer ? Alors lancez-vous ou demandez de l'aide dans les issues.
